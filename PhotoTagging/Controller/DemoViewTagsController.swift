//
//  DemoViewTagsController.swift
//  PhotoTagging
//
//  Created by Kwee Tek Yin on 25/06/19.
//  Copyright © 2019 tekyin. All rights reserved.
//

import UIKit
import SwiftyJSON
import Foundation

class ViewController: UIViewController {
    @IBOutlet weak var uPhotoTag: TYTaggableImage!

    override func viewDidLoad() {
        super.viewDidLoad()
        uPhotoTag.setEditMode(true)
        if let path = Bundle.main.path(forResource: "Photo", ofType: "json"),
           let jsonString = try? String(contentsOfFile: path, encoding: String.Encoding.utf8) {
            let json: JSON = JSON(parseJSON: jsonString)
            let products = json["data"]["photo-tagging"]["products"].arrayValue.map { (json: JSON) -> TagProductData in
                return TagProductData(json)
            }
            uPhotoTag.populateTags(products)
        } else {
            print("invalid file")
        }
        uPhotoTag.itemSelectedCallback = { (data: TagProductData) in
            print("Item selected: \(data.name)")
        }
    }
}