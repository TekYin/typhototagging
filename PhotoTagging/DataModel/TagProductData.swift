//
// Created by Kwee Tek Yin on 2019-06-25.
// Copyright (c) 2019 tekyin. All rights reserved.
//

import Foundation
import SwiftyJSON

class TagProductData {
    var idSoco:       Int
    var idSociolla:   Int
    var posX:         Double
    var posY:         Double
    var image:        String
    var name:         String
    var manufacturer: String
    var price:        Int
    var rating:       Double

    init(_ json: JSON) {
        idSoco = json["id"].intValue
        idSociolla = json["la_ps_product_id_product"].intValue
        posX = json["width_percentage"].doubleValue
        posY = json["height_percentage"].doubleValue
        image = json["image"].stringValue
        name = json["name"].stringValue
        manufacturer = json["manufacture"]["name"].stringValue
        price = json["price"].intValue
        rating = json["counter_review_rating"].doubleValue
    }
}
