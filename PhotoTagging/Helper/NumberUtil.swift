//
// Created by Kwee Tek Yin on 2019-06-25.
// Copyright (c) 2019 tekyin. All rights reserved.
//

import Foundation

class NumberUtil {
    static private var formatter: NumberFormatter = NumberFormatter()

    static func format(_ number: Int, decimal: Int = 0) -> String {
        formatter.groupingSeparator = ","
        formatter.maximumFractionDigits = decimal
        formatter.numberStyle = .decimal
        return formatter.string(for: number) ?? "0"
    }
}
