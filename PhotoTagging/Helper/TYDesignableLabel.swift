//
// Created by Tek Yin on 01/02/18.
// Copyright (c) 2018 Tek Yin. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable
public class TYDesignableLabel: UILabel {
    @IBInspectable var lineSpacing2:         CGFloat = 0
    @IBInspectable var firstLineHeadIndent2: CGFloat = 0
    @IBInspectable var headIndent2:          CGFloat = 0
    @IBInspectable var charSpacing2:         CGFloat = 0 {
        didSet {
            renderText()
        }
    }

    @IBInspectable var paddingLeft:   CGFloat = 0
    @IBInspectable var paddingRight:  CGFloat = 0
    @IBInspectable var paddingTop:    CGFloat = 0
    @IBInspectable var paddingBottom: CGFloat = 0

    override public init(frame: CGRect) {
        super.init(frame: frame)
    }

    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    public override var text: String? {
        didSet {
            self.layoutIfNeeded()
        }
    }

    public override func layoutSubviews() {
        super.layoutSubviews()
        renderText()
    }

    public func renderText() {
        if let text: String = self.text, !text.isEmpty {
            let range:      NSRange                   = NSRange(location: 0, length: text.count)
            let attrString: NSMutableAttributedString = NSMutableAttributedString(string: text)
            let style:      NSMutableParagraphStyle   = NSMutableParagraphStyle()
            if lineSpacing2 < 0 {
                let lineHeight = self.font.lineHeight
                style.maximumLineHeight = lineHeight + lineSpacing2
                style.minimumLineHeight = lineHeight + lineSpacing2
            } else {
                style.lineSpacing = lineSpacing2
            }
            style.firstLineHeadIndent = firstLineHeadIndent2
            style.headIndent = headIndent2
            style.alignment = self.textAlignment
            style.lineBreakMode = self.lineBreakMode
            attrString.addAttribute(
                    NSAttributedStringKey.paragraphStyle,
                    value: style,
                    range: range)
            attrString.addAttribute(
                    NSAttributedStringKey.font,
                    value: self.font,
                    range: range)
            attrString.addAttribute(
                    NSAttributedStringKey.foregroundColor,
                    value: self.textColor,
                    range: range)
            attrString.addAttribute(
                    NSAttributedStringKey.kern,
                    value: self.charSpacing2,
                    range: range)
            self.attributedText = attrString
        }
    }

    public override var intrinsicContentSize: CGSize {
        var intrinsicSuperViewContentSize: CGSize = super.intrinsicContentSize
        intrinsicSuperViewContentSize.height += paddingTop + paddingBottom
        intrinsicSuperViewContentSize.width += paddingLeft + paddingRight
        return intrinsicSuperViewContentSize
    }

}
