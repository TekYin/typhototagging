//
// Created by Kwee Tek Yin on 2019-06-26.
// Copyright (c) 2019 tekyin. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    func fadeIn(duration: Double = 0.3) {
        self.isHidden = false
        self.alpha = 0
        UIView.animate(withDuration: duration) { () -> Void in
            self.alpha = 1
        }
    }

    func fadeOut(duration: Double = 0.3) {
        self.isHidden = false
        self.alpha = 1
        UIView.animate(withDuration: duration,
                delay: 0,
                animations: {
                    self.alpha = 0
                },
                completion: { b in
                    self.isHidden = true
                })
    }
}