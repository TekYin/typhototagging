//
// Created by Kwee Tek Yin on 2018-11-21.
// Copyright (c) 2018 Sociolla. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    open var frameHeight: CGFloat {
        get {
            return self.frame.size.height
        }
        set {
            self.frame.size.height = newValue
        }
    }
    open var frameWidth:  CGFloat {
        get {
            return self.frame.size.width
        }
        set {
            self.frame.size.width = newValue
        }
    }
    open var frameLeft:   CGFloat {
        get {
            return self.frame.origin.x
        }
        set {
            self.frame.origin.x = newValue
        }
    }
    open var frameRight:  CGFloat {
        get {
            return self.frame.origin.x + self.frameWidth
        }
        set {
            self.frame.origin.x = newValue - self.frameWidth
        }
    }
    open var frameTop:    CGFloat {
        get {
            return self.frame.origin.y
        }
        set {
            self.frame.origin.y = newValue
        }
    }
    open var frameBottom: CGFloat {
        get {
            return self.frame.origin.y + self.frameHeight
        }
        set {
            self.frame.origin.y = newValue - self.frameHeight
        }
    }
}