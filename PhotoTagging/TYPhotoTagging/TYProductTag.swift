//
// Created by Kwee Tek Yin on 2019-06-25.
// Copyright (c) 2019 tekyin. All rights reserved.
//

import Foundation
import UIKit
import PureLayout

fileprivate enum PopupDirection {
    case topLeft
    case topRight
    case bottomLeft
    case bottomRight
}

class TYProductTag: UIView {
    @IBOutlet weak var uArrowUp:      UIImageView!
    @IBOutlet weak var uArrowDown:    UIImageView!
    @IBOutlet weak var uContainer:    UIView! {
        didSet {
            uContainer.layer.cornerRadius = 4
        }
    }
    @IBOutlet weak var uBrand:        UILabel!
    @IBOutlet weak var uName:         UILabel!
    @IBOutlet weak var uPrice:        UILabel!
    @IBOutlet weak var uRating:       UILabel!
    @IBOutlet weak var uRemoveButton: UIButton!

    var actionCallback: (() -> Void)?
    var removeCallback: (() -> Void)?
    fileprivate var currentDirection: PopupDirection = .bottomRight
    var posX: NSLayoutConstraint?
    var posY: NSLayoutConstraint?

    var showRemoveButton: Bool = false {
        didSet {
            uRemoveButton.isHidden = !showRemoveButton
        }
    }

    @IBAction func doRemoveMe(_ sender: UIButton) {
        removeCallback?()
    }

    @IBAction func doAction(_ sender: UIButton) {
        actionCallback?()
    }

    override func removeFromSuperview() {
        cleanup()
        super.removeFromSuperview()
    }

    func cleanup() {
        actionCallback = nil
        removeCallback = nil
    }

    var brand:  String = "" {
        didSet {
            uBrand.text = brand.uppercased()
        }
    }
    var name:   String = "" {
        didSet {
            uName.text = name
        }
    }
    var price:  Int    = 0 {
        didSet {
            uPrice.text = "Rp \(NumberUtil.format(price))"
        }
    }
    var rating: Double = 0 {
        didSet {
            uRating.text = "\(String(format: "%.1f", rating))"
        }
    }

    func showArrow(up: Bool) {
        uArrowDown.isHidden = up
        uArrowUp.isHidden = !up
    }

    func calculateArrow() {
        switch (currentDirection) {
            case .topLeft:
                showArrow(up: false)
                uArrowDown.frame.origin.x = self.frameWidth - 26
            case .topRight:
                showArrow(up: false)
                uArrowDown.frame.origin.x = 6
            case .bottomLeft:
                showArrow(up: true)
                uArrowUp.frame.origin.x = self.frameWidth - 26
            case .bottomRight:
                showArrow(up: true)
                uArrowUp.frame.origin.x = 6
        }
    }

    func setPosition(_ point: CGPoint) {
        guard let container = self.superview else {
            return
        }
        let showRight = point.x + self.frameWidth < container.frameWidth
        let showDown  = point.y + self.frameHeight < container.frameHeight
        if showRight {
            posX = autoPinEdge(.leading, to: .leading, of: container, withOffset: point.x - 16)
        } else {
            posX = autoPinEdge(.trailing, to: .leading, of: container, withOffset: point.x + 16)
        }
        if showDown {
            posY = autoPinEdge(.top, to: .top, of: container, withOffset: point.y)
        } else {
            posY = autoPinEdge(.bottom, to: .top, of: container, withOffset: point.y)
        }
        switch (showDown, showRight) {
            case (true, true): currentDirection = .bottomRight
            case (true, false):currentDirection = .bottomLeft
            case (false, true):currentDirection = .topRight
            case (false, false):currentDirection = .topLeft
        }

//        posY = autoPinEdge(toSuperviewEdge: .top, withInset: point.y)
        self.superview?.layoutIfNeeded()
        showArrow(up: showDown)
        calculateArrow()
    }
}
