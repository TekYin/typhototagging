//
// Created by Kwee Tek Yin on 2019-06-25.
// Copyright (c) 2019 tekyin. All rights reserved.
//

import Foundation
import UIKit
import PureLayout

@IBDesignable
class TYTaggableImage: UIView {
    var uImage:               UIImageView!
    var uToggleButton:        UIButton!
    var products:             [TagProductData] = []
    var tags:                 [UIButton]       = []
    var isShowButton:         Bool             = false
    var itemSelectedCallback: ((TagProductData) -> Void)?
    var activePopup:          TYProductTag?
    private var isEditMode: Bool = false

    private func prepare() {
        self.backgroundColor = .lightGray

        uImage = UIImageView()
        self.addSubview(uImage)
        uImage.contentMode = .scaleAspectFill
        uImage.frame = self.bounds
        uImage.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        uImage.image = UIImage(named: "placeholder")

        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.removePopup(_:)))
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        uImage.addGestureRecognizer(tapGesture)
        uImage.isUserInteractionEnabled = true

        uToggleButton = UIButton(type: .custom)
        self.addSubview(uToggleButton)
        uToggleButton.setImage(UIImage(named: "tag_icon"), for: .normal)
        uToggleButton.setTitle("See Tagged Products", for: .normal)
        uToggleButton.setTitle("Hide Tagged Products", for: .selected)
        uToggleButton.backgroundColor = UIColor(white: 0, alpha: 0.8)
        uToggleButton.layer.cornerRadius = 13
        uToggleButton.titleLabel?.font = UIFont(name: "ArialMT", size: 12)!
        uToggleButton.autoPinEdge(.leading, to: .leading, of: self, withOffset: 12)
        uToggleButton.autoPinEdge(.bottom, to: .bottom, of: self, withOffset: -12)
        uToggleButton.autoSetDimension(.height, toSize: 26)
        uToggleButton.contentEdgeInsets = UIEdgeInsets(top: 0, left: 7, bottom: 0, right: 17)
        uToggleButton.titleEdgeInsets = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: -5)
        uToggleButton.addTarget(self, action: #selector(self.toggleButtonVisibility(_:)), for: .touchUpInside)
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        prepare()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        prepare()
    }

    @objc func removePopup(_ sender: UIGestureRecognizer? = nil) {
        if let popup = activePopup {
            let id = popup.tag
            popup.removeFromSuperview()
            self.tags.first {
                $0.tag == id
            }?.isHidden = false
        }
    }

    func setEditMode(_ status: Bool) {
        isEditMode = status
    }

    @objc
    func toggleButtonVisibility(_ sender: UIButton) {
        isShowButton = !isShowButton
        uToggleButton.isSelected = isShowButton
        if !isShowButton {
            activePopup?.removeFromSuperview()
        }
        for tag in tags {
            if self.isShowButton {
                tag.fadeIn(duration: 0.2)
            } else {
                tag.fadeOut(duration: 0.2)
            }
        }
    }

    func populateTags(_ products: [TagProductData]) {
        self.layoutIfNeeded()
        isShowButton = false
        self.products = products
        for data in products {
            let tag: UIButton = UIButton(type: .custom)
            tag.setImage(UIImage(named: "tag_indicator"), for: .normal)
            tag.frame.size = CGSize(width: 20, height: 20)
            let position = getTagPosition(posX: data.posX, posY: data.posY)
            tag.frame.origin = position
            tag.addTarget(self, action: #selector(self.showDetail(sender:)), for: .touchUpInside)
            tag.tag = data.idSoco
            tag.isHidden = true
            self.addSubview(tag)
            tags.append(tag)
        }
    }

    @objc
    func showDetail(sender: UIButton) {
        removePopup()
        guard let data = products.first(where: { data in data.idSoco == sender.tag }) else {
            return
        }
        let imageWidth = uImage.frame.size.width
        let point      = getTagPosition(posX: data.posX, posY: data.posY)

        let popup: TYProductTag = UINib(nibName: "TYProductTag", bundle: nil).instantiate(withOwner: nil)[0] as! TYProductTag
        self.addSubview(popup)
        popup.brand = data.manufacturer
        popup.name = data.name
        popup.price = data.price
        popup.rating = data.rating
        popup.showRemoveButton = isEditMode
        popup.actionCallback = {
            self.itemSelectedCallback?(data)
        }
        popup.removeCallback = {
            self.products.removeAll { data in
                data.idSoco == sender.tag
            }
            self.tags.first {
                $0.tag == data.idSoco
            }?.removeFromSuperview()
            popup.removeFromSuperview()
        }
        popup.tag = sender.tag


        // calculating the position
//        let isShowDown: Bool    = point.y + popup.frame.size.height < self.frame.size.height
//        let posX:       CGFloat = min(point.x - 20, imageWidth - popup.frame.size.width - 10)
//        if isShowDown {
//            popup.autoPinEdge(toSuperviewEdge: .leading, withInset: posX)
//            popup.autoPinEdge(toSuperviewEdge: .top, withInset: point.y + 8)
//            popup.showArrow(up: true)
//        } else {
//            popup.autoPinEdge(toSuperviewEdge: .leading, withInset: posX)
//            popup.autoPinEdge(.bottom, to: .top, of: self, withOffset: point.y + 8)
//            popup.showArrow(up: false)
//        }
        self.layoutIfNeeded()
        popup.setPosition(CGPoint(x: point.x + 8, y: point.y + 8))
//        popup.calculateArrow(point.x)

        sender.isHidden = true
//        UIView.animate(withDuration: 3,
//                delay: 0, animations: {
//            popup.alpha = 0.99
//        }, completion: {
//            b in
//            popup.removeFromSuperview()
//            sender.isHidden = false
//        })
        activePopup = popup
    }

    func getTagPosition(posX: Double, posY: Double) -> CGPoint {
        let imageWidth  = Double(uImage.frame.size.width)
        let imageHeight = Double(uImage.frame.size.height)
        return CGPoint(x: posX * imageWidth / 100 - 10, y: posY * imageHeight / 100 - 10)
    }
}
